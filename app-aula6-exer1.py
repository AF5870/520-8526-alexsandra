class Onibus:

    def __init__(self):
        self.capacidade_total = 45
        self.capacidade_atual = 0
        self.movimento = False

    def embarcar(self, num_pessoas):
        if not self.movimento:
            if self.capacidade_atual + num_pessoas <= self.capacidade_total:
                self.capacidade_atual += num_pessoas
                print(f"{num_pessoas} pessoas embarcaram. Capacidade atual: {self.capacidade_atual}")
            else:
                print("Não há espaço suficiente para todas as pessoas!")
        else:
            print("Não é possível embarcar com o ônibus em movimento.")

    def desembarcar(self, num_pessoas):
        if not self.movimento:
            if self.capacidade_atual - num_pessoas >= 0:
                self.capacidade_atual -= num_pessoas
                print(f"{num_pessoas} pessoas desembarcaram . Capacidade atual: {self.capacidade_atual}")
            else:
                print("Não há pessoas suficiente no ônibus para desembarcar!")
        else:        
            print("Não é possivel desembarcar com o ônibus em movimento.")  
    def acelerar(self):
        self.movimento = True
        print("O ônibus está em movimento.")        
    def frear(self):
        self.movimento = False
        print("O ônibus parou.")

meu_onibus = Onibus()

print(meu_onibus.embarcar(5))
    
print(meu_onibus.acelerar())
    
print(meu_onibus.embarcar(10))
print(meu_onibus.frear())
print(meu_onibus.desembarcar(5))
    
print(meu_onibus.frear())
print(meu_onibus.embarcar(5))
print(meu_onibus.embarcar(15))