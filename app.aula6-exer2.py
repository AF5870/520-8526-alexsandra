class Fila:

def _init_(self): 
    self.fila = []           #Fila
    self.prioridade = []     #Apenas um registro para controle prioridade

def adicionar(self, idade):
    if idade >= 65:
        self.prioridade.append(idade)
        self.fila.append(idade)
    else:
        self.fila.append(idade)
    return(f"Uma pessoas de {} anos se juntou á fila.")

def atender(self):
    if len(self.fila) == 0:
        return("Não há ninguém na fila.")
    
    atendido = self.fila[0]
    if atendido in self.prioridade:
        self.prioridade.remove(atendido)

    self.fila.remove(atendido)
    return(f"Uma pessoa com {atendido} anos foi atendida.")

def dar_prioridade(self):
    if len(self.prioridade) == 0:
        return("Não há ninguém acima de 65 anos na fila.")
    prioritário = self.prioridade[0]
    self.prioridade.remove(prioritário)
    self.fila.remove(prioritário)
    self.fila.insert(0, prioritário)

    return(f"Uma pessoa com {prioritário} anos recebeu prioridade e passou na frente.")

banco = Fila()

print(banco.adicionar(17))
print(banco.adicionar(25))
print(banco.adicionar(82))
print(banco.adicionar(56))
print(banco.adicionar(71))
print(banco.adicionar(36))

print(banco.atender())
print(banco.dar_prioridade())
print(banco.atender())


