# Exercício 2
#  num1 = float(input("Digite o primeiro número"))
#  num2 = float(input("Digite o segundo número"))

def soma(a, b):
    return a + b
   
def diferença(a, b):
    return a - b
    
def multiplicação(a, b):
    return a * b
   
def divisão(a, b):
    if b ==0:
        return " Não é possível dividir por zero!"
    else:
        return a / b


print("Selecione a operação:")
print("1. Soma")
print("2. Diferença")
print("3. Multiplicação")
print("4. Divisão")

opcao = input("Digite o número da operação: ")
a=float(input("Digite o valor a: "))
b=float(input("Digite o valor b: "))

if opcao =="1":
    print(f"Resultado da soma é {soma(a, b)}")          
elif opcao == "2":
    print(f"Resultado da diferença é {diferença(a, b)}")
elif opcao == "3":
    print(f"Resultado da multiplicação é {multiplicação(a, b)}")
elif opcao == "4":
    print(f"Resultado da divisão é {divisão(a, b)}")
else:
    print("Opção inválida!")


