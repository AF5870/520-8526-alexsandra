# Crie um script para simular uma lanchonete.
# 
# - Essa lanchonete deverá ter 5 tipos de comida e 3 tipos de bebidas no cardápio.
# - Quando clientes chegarem a essa lanchonete, eles deverão fazer um pedido aleatório de 1 comida + 1 bebida.
# - A lanchonete deverá ter um numero limitado de porções dessas comidas/bebidas. Comida = 5, bebida = 7.
# - Quando o cliente fazer o pedido, ele deverá ser notificado se as opções escolhidas estão disponiveis,
# e quais seus preços.

# Criar um DB.
# Criar uma tabela pra comida e uma pra bebida.
# Preencher essas tabelas.

# Criar uma função pra simular um cliente comprando algo.

import sqlite3
from random import choice

conexao = sqlite3.connect("estoque.db")
cursor = conexao.cursor()

tabela_comida = """
CREATE TABLE comidas (
id integer primary key autoincrement,
nome,
preco,
quantidade
)
"""

tabela_bebida = """
CREATE TABLE bebidas (
id integer primary key autoincrement,
nome,
preco,
quantidade
)
"""

comidas = [("Hamburguer", 25.00, 5), ("Hotdog", 12.00, 5), ("Misto quente", 7.50, 5), ("Chocolate", 5.00, 5), ("Pão", 1.50, 5)]
bebidas = [("Coca cola", 10.00, 7), ("Fanta", 8.00, 7), ("Café", 2.50, 5)]

try:
    cursor.execute(tabela_comida)
    cursor.execute(tabela_bebida)

    for comida in comidas:
        registra_estoque_comida = f"""
    INSERT INTO comidas (nome, preco, quantidade) 
    VALUES ("{comida[0]}", {comida[1]}, {comida[2]})"""    
        
        cursor.execute(registra_estoque_comida)

    for bebida in bebidas:
        registra_estoque_bebida = f"""
    INSERT INTO bebidas (nome, preco, quantidade) 
    VALUES ("{bebida[0]}", {bebida[1]}, {bebida[2]})"""    
        
        cursor.execute(registra_estoque_bebida)

    conexao.commit()

except sqlite3.OperationalError:
    pass

def checa_quantidade(tabela, pedido):
    if tabela == "comidas":
        checa_estoque = f'''SELECT quantidade FROM comidas WHERE nome = "{pedido}"'''
        cursor.execute(checa_estoque)
        conteudo = cursor.fetchall()

        if conteudo[0][0] == 0:
            return False
        else:
            return conteudo[0][0]
    
    elif tabela == "bebidas":
        checa_estoque = f'SELECT quantidade FROM bebidas WHERE nome = "{pedido}"'
        cursor.execute(checa_estoque)
        conteudo = cursor.fetchall()

        if conteudo[0][0] == 0:
            return False
        else:
            return conteudo[0][0]

def checa_preco(tabela, pedido):
    if tabela == "comidas":
        checa_estoque = f'''SELECT preco FROM comidas WHERE nome = "{pedido}"'''
        cursor.execute(checa_estoque)
        conteudo = cursor.fetchall()
        return conteudo[0][0]
    
    elif tabela == "bebidas":
        checa_estoque = f'SELECT preco FROM bebidas WHERE nome = "{pedido}"'
        cursor.execute(checa_estoque)
        conteudo = cursor.fetchall()
        return conteudo[0][0]    

def cliente():
    pedido = {}
    pedido["comida"] = choice(comidas)[0]
    pedido["bebida"] = choice(bebidas)[0]

    if checa_quantidade("comidas", pedido["comida"]) == False:
        print(f'O item {pedido["comida"]} está fora de estoque.')

    elif checa_quantidade("bebidas", pedido["bebida"]) == False:
        print(f'O item {pedido["bebida"]} está fora de estoque.')

    else:
        quantidade_atual_comida = int(checa_quantidade("comidas", pedido["comida"]))
        quantidade_atual_bebida = int(checa_quantidade("bebidas", pedido["bebida"]))

        atualiza_quantidade_comida = f'UPDATE comidas SET quantidade = {quantidade_atual_comida - 1} WHERE nome = "{pedido["comida"]}"'
        atualiza_quantidade_bebida = f'UPDATE bebidas SET quantidade = {quantidade_atual_bebida - 1} WHERE nome = "{pedido["bebida"]}"'

        cursor.execute(atualiza_quantidade_comida)
        cursor.execute(atualiza_quantidade_bebida)
        conexao.commit()

        preco_comida = checa_preco("comidas", pedido["comida"])
        preco_bebida = checa_preco("bebidas", pedido["bebida"])
        preco_total = float(preco_comida) + float(preco_bebida)

        print(f'Pedido feito! {pedido["comida"]} + {pedido["bebida"]}! Preço: R${preco_total:.2f}')

for x in range(0, 30):
    cliente()
